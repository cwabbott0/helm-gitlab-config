#!/bin/bash
# vim: tabstop=2 shiftwidth=2 expandtab

set -e
set -x

# first create a file with the gitlab project name/disk_path associations
kubectl -n gitlab exec -ti \
  $(kubectl -n gitlab get pod -l app=task-runner -o name | grep -v backup) --  \
    /srv/gitlab/bin/rails runner  \
      "puts '[' ; Project.all.each() {|project| puts ' {\"gitlab\": \"' + project.full_path + '\", \"disk\": \"' + project.disk_path + '.git\", \"storage\": \"' + project.repository_storage + '\"},'} ; puts '{}]'" \
        > gitlab_storage.json

# Then merge the dump of gitlab with the dump of cgit
jq -s '[flatten | group_by(.gitlab) | map(reduce .[] as $x ({}; . * $x)) | .[] | select(.cgit != null)]' \
  sync_hooks.json gitlab_storage.json > full_sync.json

for STORAGE in no-replicas default
do
  # create the new script to send to gitaly
  cat <<EOF > sync_hooks_$STORAGE.sh
#!/bin/bash

cd /home/git/repositories
EOF

  jq -r ".[] | select(.storage == \"$STORAGE\")" full_sync.json | \
    jq -r '"DISK=\(.disk|@sh) ; if [ -e $DISK ] ; then pushd $DISK; git config --replace-all fdo.mirror-dir \(.cgit|@sh); git config --replace-all  gitlab.fullpath \(.gitlab|@sh); mkdir -p custom_hooks; ln -s /gitlab-ssh-keys/git-post-receive-mirror custom_hooks/post-receive; popd; fi"' >> sync_hooks_$STORAGE.sh
done

for POD in default-0 default-1 no-replicas-0
do
  set +x
  echo '*****************************************************************'
  echo "                      $POD"
  echo '*****************************************************************'
  set -x
  # remove the terminating '-0'
  STORAGE=$(echo $POD | sed -e 's/-.$//')
  # send it
  kubectl -n gitlab cp sync_hooks_$STORAGE.sh gitlab-prod-gitaly-$POD:/home/git/

  # run it
  kubectl -n gitlab exec -ti gitlab-prod-gitaly-$POD -- bash /home/git/sync_hooks_$STORAGE.sh
done
