#!/bin/bash

# you must set:
#  - $PACKET_PROJECT_ID to the fd.o CI Packet project
# you can set
#  - $SERVER_NAME which will default to fdo-k3s-server-1

if [ x"$PACKET_PROJECT_ID" == x"" ]
then
	echo "PACKET_PROJECT_ID not set, aborting." 1>&2
	exit 1
fi

NAME=admin-$1
SERVER_NAME=${SERVER_NAME:-fdo-k3s-server-1}

if [ "$NAME" == "admin-" ]
then
  echo "missing account name." 1>&2
  echo "usage: $0 <account_name>" 1>&2
  exit 1
fi

if [ x"$SSH_SERVER_SOCKET" == x"" ]
then
  SSH_SERVER_SOCKET=$(mktemp)
  DELETE_SOCKET="true"
fi

# get the IP of the k3s cluster
if [ x"$SERVER_IP" == x"" ]
then
  SERVER_IP=$(packet device get --project-id $PACKET_PROJECT_ID --json | jq -r '.[] | select(.hostname == "'$SERVER_NAME'") | .ip_addresses[0].address')
fi

if [ x"$SERVER_IP" == x"" ]
then
	echo "can't find $SERVER_NAME on packet" 1>&2
	exit 1
fi

persistent_ssh () {
	ssh -o ControlMaster=auto -o ControlPersist=1h -o ControlPath=$SSH_SERVER_SOCKET root@$SERVER_IP $@
}

echo "connecting to $SERVER_NAME" 1>&2
persistent_ssh "echo connected" 1>&2

persistent_ssh "kubectl -n kube-system create serviceaccount $NAME" 1>&2
persistent_ssh "kubectl create clusterrolebinding add-on-cluster-admin-$NAME \
	--clusterrole=cluster-admin \
	--serviceaccount=kube-system:$NAME 1>&2"

TOKENNAME=$(persistent_ssh "kubectl -n kube-system get serviceaccount/$NAME -o jsonpath='{.secrets[0].name}'")

CLUSTER_TOKEN=$(persistent_ssh "kubectl -n kube-system get secret $TOKENNAME -o jsonpath='{.data.token}'")

CLUSTER_CERT=$(persistent_ssh "grep certificate-authority-data /etc/rancher/k3s/k3s.yaml | cut -d ':' -f 2 | xargs")

CLUSTER_ADDRESS=$(persistent_ssh "kubectl config view --minify=true -o jsonpath='{.clusters[0].cluster.server}'")

export KUBECONFIG=$(mktemp)
TMP_K8s_CERT=$(mktemp)

kubectl config set-credentials $NAME --token=$(echo $CLUSTER_TOKEN | base64 --decode) 1>&2
echo $CLUSTER_CERT | base64 --decode > $TMP_K8s_CERT

kubectl config set-cluster $SERVER_NAME \
	--server=$CLUSTER_ADDRESS \
	--certificate-authority=$TMP_K8s_CERT \
	--embed-certs=true 1>&2

kubectl config set-context \
	admin@$SERVER_NAME \
	--cluster $SERVER_NAME \
	--user $NAME 1>&2

kubectl config use-context admin@$SERVER_NAME 1>&2

kubectl config view --minify=true --raw=true

rm $KUBECONFIG $TMP_K8s_CERT

# cleanup SSH connection
if [ x"$DELETE_SOCKET" == x"true" ]
then
  ssh -o ControlPath=$SSH_SERVER_SOCKET -O exit foo 1>&2
fi
